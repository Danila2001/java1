package ru.unn.java3;

public abstract class Figure {
    private String mName;
    private int x;
    private int y;
    private int quarter;


    public Figure(int x, int y) {
        x = x;
        y = y;
    }

    public abstract  String figureName();

    public abstract void square();

    public void getQuadrant(int x , int y) {
        if (x > 0 && y > 0) {
            quarter = 1;
            System.out.println("Четверть равна: " + quarter);
        } else if (x < 0 && y > 0) {
            quarter = 2;
            System.out.println("Четверть равна: " + quarter);
        } else if (x < 0 && y < 0) {
            quarter = 3;
            System.out.println("Четверть равна: " + quarter);
        } else if (x > 0 && y < 0) {
            quarter = 4;
            System.out.println("Четверть равна: " + quarter);
        }
    }

    public int getX() {
        return x;
    }

    public void setX() {
        this.x = x;
    }

    public int getQuarter() {
        return quarter;
    }

    public int getY() {
        return y;
    }

    public void setY() {
        this.y = y;
    }
    public String getName() {
        return mName;
    }
    public void setName() {
        this.mName = mName;
    }
}

package ru.unn.java3;


public class Main6 {
    static Figure[] figures = new Figure[5];

    public static void main(String[] args) {


        figures[0] = new Circle(-3, 5);
        figures[1] = new Figure(2, 3) {
            @Override
            public String figureName() {
                return null;
            }

            @Override
            public void square() {

            }
        };
        figures[2] = new Circle(2, 3);
        figures[3] = new Figure(2, 3) {
            @Override
            public String figureName() {
                return null;
            }

            @Override
            public void square() {

            }
        };
        figures[4] = new Circle(2, 3);
        for (int i = 0; i < 5; i++) {
            System.out.print(figures[i].figureName());
            figures[i].square();
        }

    }
}

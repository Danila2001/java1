import java.io.*;

public class Main7 {

    public static void main(String[] args) throws Exception {
        int min = (int) Math.ceil(1);
        int max = (int) Math.floor(3);
        int q = (int) (Math.floor(Math.random() * (max - min + 1)) + min);

        if (q == 1) {
            File Dir = new File("C://SE2020_LESSON9/Directory_1/");
            Dir.mkdirs();
            square();
            square_1();

        } else if (q == 2) {
            File Dir = new File("C://SE2020_LESSON9/Directory_1/Directory_2");
            Dir.mkdirs();
            square();
            square_1();
            square_2();
        } else if (q == 3) {
            File Dir = new File("C://SE2020_LESSON9/Directory_1/Directory_2/Directory_3");
            Dir.mkdirs();
            square();
            square_1();
            square_2();
            square_3();
        }




    }


    public static void square() {


        for (int i = 1; i < 4; i++) {
            int minKol = (int) Math.ceil(10);
            int maxKol = (int) Math.floor(200);
            int b = (int) (Math.floor(Math.random() * (maxKol - minKol + 1)) + minKol);

            try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream("C://SE2020_LESSON9/filename_" + i + ".txt"), "utf-8")))
            {
                for (int j = 10; j < b; j++) {
                    int a = (int) (Math.random() * 10);
                    String s = Integer.toString(a);
                    writer.write(" " + s);
                }
            }
            catch(IOException e) {
                System.out.println(e.getMessage());
            }

        }
    }
    public static void square_1() {


        for (int i = 1; i < 4; i++) {
            int minKol = (int) Math.ceil(10);
            int maxKol = (int) Math.floor(200);
            int b = (int) (Math.floor(Math.random() * (maxKol - minKol + 1)) + minKol);

            try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream("C://SE2020_LESSON9/Directory_1/filename_" + i + ".txt"), "utf-8")))
            {
                for (int j = 10; j < b; j++) {
                    int a = (int) (Math.random() * 10);
                    String s = Integer.toString(a);
                    writer.write(" " + s);
                }
            }
            catch(IOException e) {
                System.out.println(e.getMessage());
            }

        }
    }
    public static void square_2() {


        for (int i = 1; i < 4; i++) {
            int minKol = (int) Math.ceil(10);
            int maxKol = (int) Math.floor(200);
            int b = (int) (Math.floor(Math.random() * (maxKol - minKol + 1)) + minKol);

            try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream("C://SE2020_LESSON9/Directory_1/Directory_2/filename_" + i + ".txt"), "utf-8")))
            {
                for (int j = 10; j < b; j++) {
                    int a = (int) (Math.random() * 10);
                    String s = Integer.toString(a);
                    writer.write(" " + s);
                }
            }
            catch(IOException e) {
                System.out.println(e.getMessage());
            }

        }
    }
    public static void square_3() {


        for (int i = 1; i < 4; i++) {
            int minKol = (int) Math.ceil(10);
            int maxKol = (int) Math.floor(200);
            int b = (int) (Math.floor(Math.random() * (maxKol - minKol + 1)) + minKol);

            try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream("C://SE2020_LESSON9/Directory_1/Directory_2/Directory_3/filename_" + i + ".txt"), "utf-8")))
            {
                for (int j = 10; j < b; j++) {
                    int a = (int) (Math.random() * 10);
                    String s = Integer.toString(a);
                    writer.write(" " + s);
                }
            }
            catch(IOException e) {
                System.out.println(e.getMessage());
            }

        }
    }
}

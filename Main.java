package ru.unn.math;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        BufferedReader klava = new BufferedReader (new InputStreamReader(System.in));
        String f = klava.readLine();
        float n1 = Float.parseFloat(f);
        String act = klava.readLine();
        String s = klava.readLine();
        float n2 = Float.parseFloat(s);
        float res = 0;
        if(act.equals("+")){
            res = n1 + n2;
        }
        if(act.equals("-")){
            res = n1 - n2;
        }
        if(act.equals("*")){
            res = n1 * n2;
        }
        if(act.equals("/")){
            res = n1 / n2;
        }
        System.out.println(res);
    }
}
